FROM php:7.4-apache
ARG CACHEBUST=1
ENV CACHEBUST ${CACHEBUST}


RUN a2enmod rewrite \
    && a2enmod alias \
    && a2enmod authz_core \
    && a2enmod authz_host
# Copia los archivos del proyecto al directorio /var/www/html
COPY . /var/www/html

# Establece los permisos correctos para los archivos
RUN chown -R www-data:www-data /var/www/html \
    && chmod -R 755 /var/www/html

COPY 000-default.conf /etc/apache2/sites-available/000-default.conf